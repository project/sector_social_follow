#Sector Social Follow add-on

The Sector Social Follow module — part of the Starter Kit in Sector 10 — is built on the Drupal core Block module and the Drupal contribution module: Menu Block.

##Features:

<ul>
  <li><em>Follow Us</em> menu block with default menu links to the Sector project's various online spaces, is placed last in the footer region. If this does not appear at install time, please place the "Follow Us" block manually.</li>
<li>Links and visibility are customisable, using the normal Drupal menu system.</li>
  <li>No extra permissions are required.</li>
</ul>

##Administration links:

<ul>
  <li>/admin/structure/block/manage/sector_starter_followus</li>
  <li>/admin/structure/menu/manage/sector-follow-menu</li>
</ul>

<a href="https://www.sector.nz/documentation/sector-social-share-and-follow-sector-10" title="Read more">Documentation</a>
